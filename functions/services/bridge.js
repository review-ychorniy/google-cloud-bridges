const { storage: { destination } } = require('../config');
const uuid4 = require('uuid4');
const storage = require('./storage');

exports.getList = async function() {
    const result = await storage.getFiles({
        directory: destination
    });

    const [list] = result;

    return list.map(item => {
        return {
            id: item.id,
            name: item.name
        }
    })
}

exports.getItem = async function(id) {
    const file = storage.file(id);
    const [content] = await file.download();
    return JSON.parse(content.toString());
}

exports.addItem = async function(data) {
    const id = `${destination}/${uuid4()}.json`;
    const file = storage.file(id);    
    await file.save(JSON.stringify(data));
    return id;
}

exports.updateItem = async function(id, data) {
    const file = storage.file(id);
    await file.save(JSON.stringify(data));
}