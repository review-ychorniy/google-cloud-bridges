const _ = require('lodash');
const joi = require('@hapi/joi');

const rules = {
    addItem: joi.object({
        device_type: joi.string().required(),
        home_id: joi.string().required()
    }),
    updateItem: joi.object({
        device_type: joi.string(),
        home_id: joi.string()
    })
};

const make = rules => {
    function getJoiErrorParamName(joierr) {
        const wrongParamPath = _.get(joierr, "details[0].path", []);
        return (Array.isArray(wrongParamPath) ? wrongParamPath.join(" ") : "");
    }
    
    function validate({ data, schema }) {
        let error, payload;
        const { error: joierr, value } = schema.validate(data);
        if (joierr) {
            const field = getJoiErrorParamName(joierr);
            error = `Invalid param - ${field}`;
            console.warn(`Validation error: ${error}`); 
        }
        payload = value;
        return { error, payload };
    }
    
    const exportObj = { };
    _.forEach(rules, (value, key) => {
        exportObj[key] = (req, res, next) => {
            if (!(key in rules)) {
                const error = "Validation rule not found";
                res.status(500).json({ success: false, error });
                return next(error);
            }
    
            const data = req.method === "GET" ? req.query : req.body;
            const schema = rules[key];
    
            const { error, payload } = validate({ data, schema });
    
            if (error) {
                res.status(400).json({ success: false, error });
                return next(error);
            }
    
            req.payload = payload;
            return next();
        };
        exportObj[key + 'Simple'] = (data) => {
            if (!(key in rules)) {
                const error = "Validation rule not found";
                return { error };
            }
            const schema = rules[key];
            return validate({ data, schema });
        }
    });
    return exportObj;
}

module.exports = make(rules);