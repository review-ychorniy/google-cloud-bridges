const admin = require("firebase-admin");
const config = require('../config');

admin.initializeApp({
    credential: admin.credential.cert(config.storage.serviceAccount),
    storageBucket: config.storage.bucket
});

module.exports = admin.storage().bucket();

