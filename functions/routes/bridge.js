const router = require('express').Router();
const ctrl = require('../ controllers/bridge');
const validators = require('../services/validators');

router.get('/', ctrl.getList);
router.get('/:id', ctrl.checkItem, ctrl.getItem);
router.post('/', validators.addItem, ctrl.addItem);
router.put('/:id', validators.updateItem, ctrl.checkItem, ctrl.updateItem);

module.exports = router;
