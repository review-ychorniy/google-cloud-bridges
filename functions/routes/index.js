const router = require('express').Router();
const brige = require('./bridge');

router.use(brige);

module.exports = router;