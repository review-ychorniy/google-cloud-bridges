const moment = require('moment');
const bridgeSrv = require('../services/bridge');

exports.getList = async function(req, res) {
    let list;

    try{
        list = await bridgeSrv.getList();
    } catch (err) {
        const error = 'Failed get bridge list';
        console.error(error, err);
        res.status(500).send({ success: false, error });
        return;
    }
    
    res.send({ success: true, list });
}

exports.checkItem = async function(req, res, next) {
    const { params: { id } } = req;

    let item;

    try {
        item = await bridgeSrv.getItem(decodeURIComponent(id));
    } catch (err) {
        let error;
        const code = err.code || 500;

        switch(code) {
            case 404: {
                error = 'Brige not found';
                break;
            }
            default: {
                error = 'Failed get bridge item';
                break;
            }
        }

        console.error(error, err);
        res.status(code).send({ success: false, error });
        return;
    }

    req.item = item;
    req.id = id;
    next();
}

exports.getItem = async function(req, res) {
    const { item } = req;
    res.send({ success: true, item });
}

exports.addItem = async function(req, res) {
    const { payload } = req;
    
    const data = Object.assign({}, payload, {
        last_update: moment.utc().unix()
    });

    let id;

    try{
        id = await bridgeSrv.addItem(data);
    } catch (err) {
        const error = 'Failed create bridge item';
        console.error(error, err);
        res.status(500).send({ success: false, error });
        return;
    }

    res.send({ success: true, itemId: id })
}

exports.updateItem = async function(req, res) {
    const { payload, item, id } = req;
    
    const data = Object.assign({}, item, payload, {
        last_update: moment.utc().unix()
    });

    try{
        await bridgeSrv.updateItem(id, data);
    } catch (err) {
        const error = 'Failed update bridge item';
        console.error(error, err);
        res.status(500).send({ success: false, error });
        return;
    }
    
    res.send({ success: true })
}