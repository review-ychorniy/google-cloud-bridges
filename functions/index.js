const functions = require('firebase-functions');
const expres = require('express');
const cors = require('cors');

const routes = require('./routes');
const app = expres();

app.use(cors());
app.use(routes);

exports.bridges = functions.https.onRequest(app);
